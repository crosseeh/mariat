import RPi.GPIO as GPIO
from time import sleep, time
import sys
from subprocess import call
from multiprocessing import Process
import cwiid
import random

forward = [
    (1,  0,  0,  0),
    (1,  1,  0,  0),
    (0,  1,  0,  0),
    (0,  1,  1,  0),
    (0,  0,  1,  0),
    (0,  0,  1,  1),
    (0,  0,  0,  1),
    (1,  0,  0,  1)
]
backward = forward[::-1]


rightMotor = (27, 22, 10, 9)
leftMotor = (2, 3, 4, 17)
rightBumper = 12
leftBumper = 16
tail = 21
frontLed = 20
backLed = 23
enableButton = 18

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(rightMotor, GPIO.OUT)
GPIO.setup(leftMotor, GPIO.OUT)
GPIO.setup(frontLed, GPIO.OUT)
GPIO.setup(backLed, GPIO.OUT)
GPIO.setup(rightBumper, GPIO.IN)
GPIO.setup(leftBumper, GPIO.IN)
GPIO.setup(tail, GPIO.IN)
GPIO.setup(enableButton, GPIO.IN)



def main():
    soundPlayed = False
    enabled = False
    enabledDelay = time()
    streak = 0
    lost = False
    lostStreak = 0


    while True:
        while True:
            while wm.state['buttons'] == 128:
                if time() > enabledDelay:
                    if enabled:
                        enabledDelay = time() + 0.5
                        enabled = False
                    else:
                        enabledDelay = time() + 0.5
                        enabled = True
                else:
                    continue

            if enabled == False:
                continue

            while wm.state['buttons'] == 256:
                turnButtonLinks()

            while wm.state['buttons'] == 512:
                turnButtonRechts()

            while wm.state['buttons'] == 1024:
                turnButtonAchter()

            if GPIO.input(tail) == 1 and lost == False:
                if lostStreak > 175:
                    lost = True
                lostStreak += 1
                goForward()
                continue

            if lost == True:
                if soundPlayed == False:
                    p = Process(target=playSound, args=("waa",))
                    p.daemon = True
                    p.start()
                    soundPlayed = True

                if GPIO.input(rightBumper) == 0 or GPIO.input(leftBumper) == 0:
                    if streak > 20000:
                        break
                    streak +=1
                    continue

                if GPIO.input(rightBumper) == 1 and GPIO.input(leftBumper) == 1:
                    streak = 0

                goForward()
                continue



            elif GPIO.input(tail) == 0 :
                lostStreak = 0
                if GPIO.input(rightBumper) == 0:
                    p = Process(target=playSound, args=("bamhv",))
                    p.daemon = True
                    p.start()
                    l = Process(target=blink)
                    l.daemon = True
                    l.start()
                    goBack()
                    turn180c()
                elif GPIO.input(leftBumper) == 0:
                    p = Process(target=playSound, args=("bamhv",))
                    p.daemon = True
                    p.start()
                    l = Process(target=blink)
                    l.daemon = True
                    l.start()
                    goBack()
                    turn180cc()
                else:
                    goForward()

        p = Process(target=playSound, args=("auw",))
        p.daemon = True
        p.start()

        r = Process(target=rumble)
        r.daemon = True
        r.start()

        l = Process(target=blink)
        l.daemon = True
        l.start()

        while True:
            if wm.state['buttons'] == 128:
                if time() > enabledDelay:
                    enabledDelay = time()
                    enabled = False
                    soundPlayed = False
                    lost = False
                    break


def turn180c():
    turnDelay = time() + (random.randrange(3000, 5000) / 1000)
    while True:
        for x in range(8):
            GPIO.output(rightMotor, backward[x])
            GPIO.output(leftMotor, forward[x])
            sleep(0.001)
        if time() > turnDelay:
            break


def turn180cc():
    turnDelay = time() + (random.randrange(3000, 5000) / 1000)
    while True:
        for x in range(8):
            GPIO.output(rightMotor, forward[x])
            GPIO.output(leftMotor, backward[x])
            sleep(0.001)
        if time() > turnDelay:
            break


def goBack():
    backDelay = time() + 1.5
    while True:
        for x in range(8):
            GPIO.output(rightMotor, backward[x])
            GPIO.output(leftMotor, backward[x])
            sleep(0.001)
        if time() > backDelay:
            break


def goForward():
    for x in range(8):
        GPIO.output(rightMotor, forward[x])
        GPIO.output(leftMotor, forward[x])
        sleep(0.001)


def turnButtonRechts():
    for x in range(8):
        GPIO.output(rightMotor, backward[x])
        GPIO.output(leftMotor, forward[x])
        sleep(0.001)


def turnButtonLinks():
    for x in range(8):
        GPIO.output(rightMotor, forward[x])
        GPIO.output(leftMotor, backward[x])
        sleep(0.001)


def turnButtonAchter():
    for x in range(8):
        GPIO.output(rightMotor, backward[x])
        GPIO.output(leftMotor, backward[x])
        sleep(0.001)

def blink():
    GPIO.output(frontLed,  0)
    GPIO.output(backLed,  0)
    for x in range(10):
        GPIO.output(frontLed,  0)
        GPIO.output(backLed,  1)
        sleep(0.1)
        GPIO.output(frontLed,  1)
        GPIO.output(backLed,  0)
        sleep(0.1)
    GPIO.output(frontLed,  1)
    GPIO.output(backLed,  1)

def playSound(file):
    #call(["aplay", f"GameOver.wav"])
    pass


def rumble():
    global wm
    wm.rumble = 1
    sleep(1)
    wm.rumble = 0

GPIO.output(frontLed,  0)
GPIO.output(backLed,  0)
while True:
    try:
        wm = cwiid.Wiimote(bdaddr="00:19:1D:94:15:D4")
        # wm = cwiid.Wiimote()
        break
    except RuntimeError:
        print("Cannot connect to your Wiimote. Try again...")
        continue

GPIO.output(frontLed,  1)
GPIO.output(backLed,  1)
p = Process(target=playSound, args=("yoshi",))
p.daemon = True
p.start()

wm.rpt_mode = cwiid.RPT_BTN
wm.led = 9

if __name__ == "__main__":
    main()
