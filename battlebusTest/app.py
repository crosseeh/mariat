# =====================================================
# Main control panel for the killerbot application. From
# here commands should be given to the kart. For now this
# is manual, but later a bluetooth remote will be implemented
# from which the kart will be controlled.
#
# TODO:
# add remote control
# ...
#
# =====================================================
from Behavior import *


def main():
    battlebus = Behavior()
    battlebus.ai()

if __name__ == '__main__':
    main()
