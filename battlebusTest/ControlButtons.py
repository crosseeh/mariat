import RPi.GPIO as GPIO  # import library for working with Raspberry's GPIO
import pygame
import time


class ControlButtons:
    def __init__(self):
        # Robot buttons initialization
        self.buttonPin1 = 16  # front left button
        self.buttonPin2 = 12  # front right button
        self.buttonPin3 = 21  # backside button

        self.prev_state1 = 1  # set start state to 1 (button released)
        self.prev_state2 = 1  # set start state to 1 (button released)
        self.prev_state3 = 1  # set start state to 1 (button released)

        # Pygame initialization
        pygame.init()
        joystick_count = pygame.joystick.get_count()
        for i in range(joystick_count):
            self.joystick = pygame.joystick.Joystick(i)
            self.joystick.init()
        self.buttons = self.joystick.get_numbuttons()

        # Controller buttons initialization
        # !! TODO: Checken of de knoppen kloppen
        self.closeButton = 1 # Button assignment to check user exit (O)
        self.startButton = 0 # Button assignment to check user start (X) (is 99% zeker niet de goede)


    def setup(self):
        # we're using the BCM pin layout of the Raspberry PI
        GPIO.setmode(GPIO.BCM)
        # set pin GPIO4 to be an input pin; this pin will read the button state
        # activate pull down for pin GPIO4
        GPIO.setup(self.buttonPin1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.buttonPin2, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.buttonPin3, GPIO.IN, pull_up_down=GPIO.PUD_UP)


    def checkUserExit(self):
        #Check if the user presses the close button
        # !! TODO: Dit verbeteren. heb dit overgenomen van mn KartMetController
        # script, maar vgm werkt t voor geen meter xd
        for i in range(self.buttons):
            button = self.joystick.get_button(i)
            if(i == 2 and button == self.closeButton):
                return True
        return False

    def checkUserStart(self):
        #Check if user presses the start button
        for i in range(self.buttons):
            button = self.joystick.get_button(i)
            # !! TODO:  i == 2 moet sws aangepast worden
            if(i == 2 and button == self.startButton):
                return True
        return False

    def checkRobotButtonPressRelease(self, button):
        self.setup()

        # !! TODO: dit voor alle drie de knoppen doen
        # curr_state2 = GPIO.input(self.buttonPin2)
        # curr_state1 = GPIO.input(self.buttonPin1)
        if (button == "back"):
            curr_state3 = GPIO.input(self.buttonPin3)
            self.button3(curr_state3, self.prev_state3)
        # when exiting, reset all pins
        GPIO.cleanup()

    def buttonClicks(self, button):
        self.setup()
        event = 1
        ##buttonPin3.when_pressed = sound.play
        ##GameOver = pygame.mixer.Sound
        # keep on executing this loop forever (until someone stops the program)
        # while True:
        if (button == "back"):
            print(button)
            curr_state3 = GPIO.input(self.buttonPin3)
            print(curr_state3)
            self.button3(curr_state3, self.prev_state3)

            # curr_state2 = GPIO.input(self.buttonPin2)
            # curr_state1 = GPIO.input(self.buttonPin1)

            # if (curr_state1 != self.prev_state1):  # state changed from '1' to '0' or from '0' to '1'
            #     self.button1(curr_state1, self.prev_state1)
            #
            # elif (curr_state2 != self.prev_state2):  # state changed from '1' to '0' or from '0' to '1'
            #     self.button2(curr_state2, self.prev_state2)
            # elif (curr_state3 != self.prev_state3):  # state changed from '1' to '0' or from '0' to '1'
            #     self.button3(curr_state3, self.prev_state3)
            ##GameOver.play()
            ##print("playing")

        # when exiting, reset all pins
        GPIO.cleanup()

    def button1(self, curr_state1, prev_state1):
        if (curr_state1 == 1):  # button changed from pressed ('0') to released ('1')
            event = "button 1 released"
            print(event)  # print event to console
        else:  # button changed from released ('1') to pressed ('0')
            event = "button 1 pressed"  # print event to console
            print(event)
        prev_state1 = curr_state1  # store current state
        time.sleep(0.02)  # sleep for a while, to prevent bouncing

    def button2(self, curr_state2, prev_state2):
        if (curr_state2 == 1):  # button changed from pressed ('0') to released ('1')
            event = "button 2 released"
            print(event)  # print event to console
        else:  # button changed from released ('1') to pressed ('0')
            event = "button 2 pressed"  # print event to console
            print(event)
        prev_state2 = curr_state2  # store current state
        time.sleep(0.02)  # sleep for a while, to prevent bouncing

    # Button on the back
    def button3(self, curr_state3, prev_state3):
        if (curr_state3 == 1):  # button changed from pressed ('0') to released ('1')
            event = "button 3 released"
            # print (event)  # print event to console
            # print("GAME OVER! :( ")
            # !! TODO: DIT OMDRAAIEN
            return True
        else:  # button changed from released ('1') to pressed ('0')
            event = "button 3 pressed"  # print event to console
            # print (event)
            # !! TODO: DIT OMDRAAIEN
            return False
        prev_state3 = curr_state3  # store current state
        time.sleep(0.02)  # sleep for a while, to prevent bouncing
