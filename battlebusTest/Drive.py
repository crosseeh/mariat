import RPi.GPIO as GPIO  # import library for working with Raspberry's GPIO
from ControlButtons import *
from Unit_Conversion import *
import sys 
sys.setrecursionlimit(10**6)
class Drive():
    def __init__(self):
        # READ IN BCM: THESE ARE THE GPIO NUMBERS
        # NOT THE PIN NUMBERS ON THE RASPBERRY PI
        self.gpio_pins_left = [2, 3, 4, 17]
        self.gpio_pins_right = [27, 22, 10, 9]
        self.active_coil_matrix = [ [1, 0, 0, 0],   # Sequence in which coils in the electric
                                    [1, 1, 0, 0],   # motor are powered. The elements represent
                                    [0, 1, 0, 0],   # the pins in gpio_pins_(left/right).
                                    [0, 1, 1, 0],
                                    [0, 0, 1, 0],
                                    [0, 0, 1, 1],
                                    [0, 0, 0, 1],
                                    [1, 0, 0, 1] ]
        self.net_x = 0
        self.net_y = 0
        self.controlButtons = ControlButtons()
        self.kart_facing = ""
        self.un_conv = Unit_Conversion()
        # Pygame initialization
        pygame.init()
        self.joystick_count = pygame.joystick.get_count()
        for i in range(self.joystick_count):
            self.joystick = pygame.joystick.Joystick(i)
            self.joystick.init()
            self.hats = self.joystick.get_numhats()
        self.buttons = self.joystick.get_numbuttons()



    # Initializes the GPIO pins connected to the motor as an output
    def setup(self):
        GPIO.setmode(GPIO.BCM)
        gpio_pins = [2, 3, 4, 17, 27, 22, 10, 9]
        for pin in gpio_pins:
            GPIO.setup(pin, GPIO.OUT)

    def update_position(self, facing_to):
        if facing_to == "forward":
            self.net_y += 1
        elif facing_to == "forward_left":
            self.net_x -= 0.707106781
            self.net_y += 0.707106781
        elif facing_to == "left":
            self.net_x -= 1
        elif facing_to == "back_left":
            self.net_x -= 0.707106781
            self.net_y -= 0.707106781
        elif facing_to == "back":
            self.net_y -= 1
        elif facing_to == "back_right":
            self.net_x += 0.707106781
            self.net_y -= 0.707106781
        elif facing_to == "right":
            self.net_x += 1
        elif facing_to == "forward_right":
            self.net_x += 0.707106781
            self.net_y += 0.707106781
        else:
            self.net_x += 0
            self.net_y += 0

    # Used for user input
    def move(self):
        # Get the arrow keys
        for i in range(self.joystick_count):
            joystick = pygame.joystick.Joystick(i)
            joystick.init()
            self.hats = joystick.get_numhats()

        # Hat position. All or nothing for direction, not a float like
        # get_axis(). Position is a tuple of int values (x, y).
        # (-1, 0 ) = left
        # (1, 0) = right
        # (0, 1) = up
        # (0, -1) = down
        for x in range(self.hats):
            hat = self.joystick.get_hat(x)
            # Rotate to the left
            if hat == (-1, 0):
                self.driveManually("left")
                return True
            # Rotate to the right
            elif hat == (1, 0):
                self.driveManually("right")
                return True
            # Move forward
            elif hat == (0, 1):
                self.driveManually("forward")
                return True
            # Move backward
            elif hat == (0, -1):
                self.driveManually("back")
                return True
        return False

    def update_wheel(self, direction, facing):
        for step in range(8):
            for pin in range(4):
                if direction.lower() == "forward":
                    GPIO.output(self.gpio_pins_left[pin], self.active_coil_matrix[4 - step][pin])         # Sequences like normal
                    GPIO.output(self.gpio_pins_right[pin], self.active_coil_matrix[step][pin])            # Sequences in reverse, since right wheel is mirrored: net forward
                    self.update_position(facing)
                elif direction.lower() == "back":
                    GPIO.output(self.gpio_pins_left[pin], self.active_coil_matrix[step][pin])             # Reverse of the
                    GPIO.output(self.gpio_pins_right[pin], self.active_coil_matrix[4 - step][pin])        # forward movement
                    self.update_position(facing)
                elif direction.lower() == "right":
                    GPIO.output(self.gpio_pins_left[pin], self.active_coil_matrix[4 - step][pin])         # Both wheels moving opposite direction,
                    GPIO.output(self.gpio_pins_right[pin], self.active_coil_matrix[4 - step][pin])        # causing the kart to turn in its place (right)
                    self.update_position(facing)
                elif direction.lower() == "left":
                    GPIO.output(self.gpio_pins_left[pin], self.active_coil_matrix[step][pin])             # Both wheels moving opposite direction,
                    GPIO.output(self.gpio_pins_right[pin], self.active_coil_matrix[step][pin])            # Causing the kart to turn in its place (left)
                    self.update_position(facing)
            time.sleep(0.00075)

    # Makes a wheel spin in a certain direction (clockwise / counterclockwise)
    # Called when the AI function starts
    def driveAutonomous(self, indices, direction="forward", facing=""):
        self.setup()
        for i in range(indices):
            # Check user input
            while self.move():
                self.driveManually(direction)

            # Check if balloon is not popped
            #!! TODO: Dit naar while not veranderen, nu checkt ie dat ie RELEASED is
            while self.controlButtons.checkRobotButtonPressRelease("back"):
                # If neither user input is detected or the balloon is popped, drive
                self.update_wheel(direction)

    # Makes a wheel spin in a certain direction (clockwise / counterclockwise)
    # Called when user takes control
    def driveManually(self, direction, facing=""):
        self.setup()
        while self.controlButtons.checkRobotButtonPressRelease("back"):
            # If neither user input is detected or the balloon is popped, drive
            self.update_wheel(direction)

    def return_to_start(self):
        x_index = int(self.net_x / 32)
        y_index = int(self.net_y / 32)
        # backtrack x en y index
        if x_index < 0:
            self.driveAutonomous(self.un_conv.deg_to_index(self.recovery_dict[self.kart_facing]), "right")
            self.kart_facing = "right"
            self.driveAutonomous(-x_index, "forward", self.kart_facing)
            if y_index < 0:
                self.driveAutonomous(self.un_conv.deg_to_index(90), "left")
                self.kart_facing = "forward"
                self.driveAutonomous(-y_index, "forward", self.kart_facing)
            elif y_index > 0:
                self.driveAutonomous(self.un_conv.deg_to_index(90), "right")
                self.kart_facing = "back"
                self.driveAutonomous(y_index, "forward", self.kart_facing)
        elif x_index > 0:
            self.driveAutonomous(self.un_conv.deg_to_index(self.recovery_dict[self.kart_facing]), "left")
            self.kart_facing = "left"
            self.driveAutonomous(x_index, "forward", self.kart_facing)
            if y_index < 0:
                self.drive.driveAutonomous(self.un_conv.deg_to_index(90), "right")
                self.kart_facing = "forward"
                self.drive.driveAutonomous(-y_index, "forward", self.kart_facing)
            elif y_index > 0:
                self.driveAutonomous(self.un_conv.deg_to_index(90), "left")
                self.kart_facing = "back"
                self.driveAutonomous(y_index, "forward", self.kart_facing)
        if self.kart_facing == "forward":
            self.driveAutonomous(self.un_conv.deg_to_index(90), "right")
        elif self.kart_facing == "back":
            self.driveAutonomous(self.un_conv.deg_to_index(90), "left")
        self.kart_facing = "right"
        self.driveAutonomous(self.un_conv.cm_to_index(50), "forward")
