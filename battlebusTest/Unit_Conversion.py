class Unit_Conversion:
    def cm_to_index(self, distance_in_cm):
        return int(distance_in_cm * 24)


    def s_to_index(self, time_in_s):
        return int(time_in_s * 100)


    def deg_to_index(self, angle_in_deg):
        return int(angle_in_deg * 3.5)


    def index_to_cm(self, indices):
        return indices / 24


    def index_to_s(self, indices):
        return indices / 100


    def index_to_deg(self, indices):
        return indices / 3.5


    def calc_velocity(self, indices):
        a = self.index_to_distance_cm(indices)
        b = self.index_to_time_s(indices)
        return a / b
