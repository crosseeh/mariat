from Unit_Conversion import *
from Drive import *
from ControlButtons import *
from random import randint

class Behavior:
    def __init__(self):
        self.controlButtons = ControlButtons()
        self.drive = Drive()
        self.un_conv = Unit_Conversion()
        self.directions = ["forward", "forward_left", "left", "back_left", "back", "back_right", "right", "forward_right"]
        self.recovery_dict = {"forward": 90, "forward_left": 135, "left": 180, "back_left": 225, "back": 270, "back_right": 315, "right": 0, "forward_right": 45}
        self.kart_facing = ""

    def test(self):
        self.drive.driveAutonomous(self.un_conv.deg_to_index(90), "right")
        self.kart_facing = "right"
        self.drive.driveAutonomous(self.un_conv.cm_to_index(50), "forward", self.kart_facing)
        back_button = True
        while back_button:
            back_button = self.standard()
        self.return_to_start()

    def ai(self):
        self.drive.driveAutonomous(self.un_conv.deg_to_index(90), "right")
        self.kart_facing = "right"
        self.drive.driveAutonomous(self.un_conv.cm_to_index(50), "forward", self.kart_facing)
        behaviors = ["standard", "attack", "defend"]
        self.listen_to_behavior(behaviors)
        self.drive.return_to_start()

    def listen_to_behavior(self, behaviors):
        back_button = True
        user_input = False
        while not back_button: #and not user_input:
            back_button = self.controlButtons.buttonClicks("back")
            user_input = self.drive.move()
            random_index = randint(0, len(behaviors) - 1)
            behavior = behaviors[random_index]
            if behavior == "standard":
                print("Standard behavior")
                back_button = self.standard()
            elif behavior == "attack":
                print("Attack behavior")
                back_button = self.attack()
            elif behavior == "defend":
                print("Defend behavior")
                self.defend()

    def waitForUserInput(self):
        userStart = False
        while not userStart:
            self.buttons.checkUserStart()
        self.ai()

    def interrupt(self):
        interrupt = self.controlButtons.buttonClicks("back")
        return interrupt

    def standard(self):
        for i in range(8):
            if not self.interrupt():
                return False
                break
            index = self.directions.find(self.kart_facing)
            if index + 1 > 7:
                index = -1
            self.drive.driveAutonomous(self.un_conv.deg_to_index(45), "left")
            self.kart_facing = self.directions[index + 1]
            self.drive.driveAutonomous(self.un_conv.cm_to_index(100), "forward", self.kart_facing)   # verander afstand naar 100 ipv 10
        return True

    def attack(self):
        for i in range(8):
            (self.un_conv.deg_to_index(45), "left")
            if index + 1 > 7:
                index = -1
            self.kart_facing = self.directions[index + 1]
            index = self.directions.find(self.kart_facing)
            self.drive.driveAutonomous(self.un_conv.cm_to_index(50), "forward", self.kart_facing)
            self.drive.driveAutonomous(self.un_conv.deg_to_index(90), "left")
            if index + 2 > 7 and index + 1 > 7:
                index = -1
            elif index + 2 > 7 and not index + 1 > 7:
                index = -2
            self.kart_facing = self.directions[index + 2]
            index = self.directions.find(self.kart_facing)
            self.drive.driveAutonomous(self.un_conv.cm_to_index(250), "forward", self.kart_facing)
            self.drive.driveAutonomous(self.un_conv.deg_to_index(90), "left")
            if index + 2 > 7 and index + 1 > 7:
                index = -1
            elif index + 2 > 7 and not index + 1 > 7:
                index = -2
            self.kart_facing = self.directions[index + 2]
            index = self.directions.find(self.kart_facing)
            self.drive.driveAutonomous(self.un_conv.cm_to_index(50), "forward", self.kart_facing)

    def defend(self):
        self.drive.driveAutonomous(self.un_conv.deg_to_index(90), "left")
        time.sleep(5)
        self.drive.driveAutonomous(self.un_conv.deg_to_index(90), "right")
