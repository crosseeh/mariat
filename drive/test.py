import RPi.GPIO as gpio
import time

def init():
    gpio.setmode(gpio.BOARD)

    # Linker motor
    gpio.setup(3, gpio.OUT)
    gpio.setup(5, gpio.OUT)
    gpio.setup(7, gpio.OUT)
    gpio.setup(11, gpio.OUT)

    # Rechter motor
    gpio.setup(13, gpio.OUT)
    gpio.setup(15, gpio.OUT)
    gpio.setup(19, gpio.OUT)
    gpio.setup(21, gpio.OUT)

def forward(tf):
    init()
    # Linker motor
    gpio.output(3, gpio.OUT)
    gpio.output(5, gpio.OUT)
    gpio.output(7, gpio.OUT)
    gpio.output(11, gpio.OUT)

    # Rechter motor
    gpio.output(13, gpio.OUT)
    gpio.output(15, gpio.OUT)
    gpio.output(19, gpio.OUT)
    gpio.output(21, gpio.OUT)
    time.sleep(2)
    gpio.cleanup()

forward(1)
