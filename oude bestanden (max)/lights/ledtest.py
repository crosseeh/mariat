from gpiozero import LED
from time import sleep


def ledgreen():
    ledGroen = LED(24) #define green led
    ledstripG = LED(20) #define ledstrip green
    while True:
        ledGroen.on() #turn green led on while in game
        ledstripG.on()
    
def ledred():
    ledRood  = LED(23) #define red led
    ##ledstripR = LED() #define red ledstrip
    for gameover in range(20):
        ledRood.on() #red led on
        sleep(0.5) #keep on for 0.5 second
        ledRood.off() #red led on
        sleep(0.5) #keep off for 0.5 second
    
def main():
    ledgreen()
    #ledred()
    print("running")

if __name__ == "__main__":
    main()

