import RPi.GPIO as GPIO  # import library for working with Raspberry's GPIO
import time# needed to use sleep()
##import pygame 
##pygame.mixer.pre_init(44100, 16, 2, 4096) #frequency, size, channels, buffersize
##pygame.init() #turn all of pygame on.
GPIO.setwarnings(False)

# global variables
buttonPin1 = 16  # voorkant links
buttonPin2 = 12  # voorkant rechts
buttonPin3 = 21  # achterkant
##buttonPin4 = 26  # Buzzer
# in this case pin GPIO4 (which is pin number 7)
prev_state1 = 1  # set start state to 1 (button released)
prev_state2 = 1  # set start state to 1 (button released)
prev_state3 = 1  # set start state to 1 (button released)
#Play the sound when button 3 is released:
##GameOver = pygame.mixer.Sound('GameOver.wav')
def lights():
    GPIO.setwarnings(False)
    GPIO.setup(18, GPIO.OUT)
    
     
    GPIO.setup(18,GPIO.HIGH)
    time.sleep(2)
    GPIO.setup(18,GPIO.LOW)
    time.sleep(2)
    GPIO.setup(18,GPIO.HIGH)
    time.sleep(2)
    GPIO.setup(18,GPIO.LOW)
def setup():
    # we're using the BCM pin layout of the Raspberry PI
    GPIO.setmode(GPIO.BCM)
    # set pin GPIO4 to be an input pin; this pin will read the button state
    # activate pull down for pin GPIO4
    GPIO.setup(buttonPin1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(buttonPin2, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(buttonPin3, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    
    
def sound():
    GPIO.setwarnings(False)
    GPIO.setup(26, GPIO.OUT)
    for i in range(6):
        GPIO.setup(26,GPIO.HIGH)
        time.sleep(0.1)
        GPIO.setup(26,GPIO.LOW)
        time.sleep(0.3)
    return
    
def buttonClicks():
    setup()
    event = 1
    ##buttonPin3.when_pressed = sound.play
    ##GameOver = pygame.mixer.Sound
    # keep on executing this loop forever (until someone stops the program)
    while True:
        curr_state1 = GPIO.input(buttonPin1)
        curr_state2 = GPIO.input(buttonPin2)
        curr_state3 = GPIO.input(buttonPin3)

        if (curr_state1 != prev_state1):  # state changed from '1' to '0' or from '0' to '1'
            button1(curr_state1, prev_state1)
            
        elif (curr_state2 != prev_state2):  # state changed from '1' to '0' or from '0' to '1'
            button2(curr_state2, prev_state2)
        elif (curr_state3 != prev_state3):  # state changed from '1' to '0' or from '0' to '1'
            button3(curr_state3, prev_state3)
            sound()
            ##GameOver.play()
            ##print("playing")

    # when exiting, reset all pins
    GPIO.cleanup()


def button1(curr_state1, prev_state1):
    if (curr_state1 == 1):  # button changed from pressed ('0') to released ('1')
        event = "button 1 released"
        print (event)  # print event to console
    else:  # button changed from released ('1') to pressed ('0')
        event = "button 1 pressed"  # print event to console
        print (event)
    prev_state1 = curr_state1  # store current state
    time.sleep(0.02)  # sleep for a while, to prevent bouncing


def button2(curr_state2, prev_state2):
    if (curr_state2 == 1):  # button changed from pressed ('0') to released ('1')
        event = "button 2 released"
        print (event)  # print event to console
    else:  # button changed from released ('1') to pressed ('0')
        event = "button 2 pressed"  # print event to console
        print (event)
    prev_state2 = curr_state2  # store current state
    time.sleep(0.02)  # sleep for a while, to prevent bouncing


def button3(curr_state3, prev_state3):
    if (curr_state3 == 1):  # button changed from pressed ('0') to released ('1')
        event = "button 3 released"
        print (event)  # print event to console
        print("GAME OVER! :( ")
        sound()
    else:  # button changed from released ('1') to pressed ('0')
        event = "button 3 pressed"  # print event to console
        print (event + "in game")
        sound()
    prev_state3 = curr_state3  # store current state
    time.sleep(0.02)  # sleep for a while, to prevent bouncing


def main():
    lights()
    buttonClicks()


if __name__ == "__main__":
    main()
