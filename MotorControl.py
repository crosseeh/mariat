import RPi.GPIO as GPIO
import time


# READ IN BCM: THESE ARE THE GPIO NUMBERS
# NOT THE PIN NUMBERS ON THE RASPBERRY PI
gpio_pins_left = [2, 3, 4, 17]
gpio_pins_right = [27, 22, 10, 9]

active_coil_matrix = [ [0, 1, 1, 1],    # Sequence in which coils in the electric
                       [0, 0, 1, 1],    # motor are powered. The elements represent
                       [1, 0, 1, 1],    # the pins in gpio_pins_(left/right).
                       [1, 0, 0, 1],
                       [1, 1, 0, 1],
                       [1, 1, 0, 0],
                       [1, 1, 1, 0],
                       [0, 1, 1, 0] ]


# =====
# main
# =====
def main():
    setup()
    test_drive()
    GPIO.cleanup()


def drive_for_second():
    drive(100)


def test_drive():           # movement prediction === this is just a prediction
    drive(1024)             # moves forward approx. 40.2 cm     (net distance traveled: 40.1 cm forward, facing: forward)
    drive(512, "back")      # moves backward approx. 20.1 cm    (net distance traveled: 20.1 cm forward, facing: forward)
    drive(320, "left")      # moves left on a 90 degree angle   (net distance traveled: 20.1 cm forward, facing: left)
    drive(320, "right")     # moves right on a 90 degree angle  (net distance traveled: 36.1 cm forward, 16 cm left, facing: forward)
    drive(640, "rotate")    # rotates 180 degrees in its place  (net distance traveled: 36.1 cm forward, 16 cm left, facing: backward)
    drive(320, "right")     # moves left on a 90 degree angle   (net distance traveled: 36.1 cm forward, 16 cm left, facing: right)
    drive(320, "left")      # moves left on a 90 degree angle   (net distance traveled: 20.1 cm forward, facing: backward)
    drive(512)              # moves forward approx. 20.1 cm     (net distance traveled: 0 cm forward, facing backward)
    drive(640, "rotate")    # rotates 180 degrees in its place  (net distance traveled: start)


def setup():
    GPIO.setmode(GPIO.BCM)
    gpio_pins = [2, 3, 4, 17, 27, 22, 10, 9]

    for pin in gpio_pins:
        GPIO.setup(pin, GPIO.OUT)


# ======================== For duration_indices ========================
# 100 indices = approx. 1 second
# 24 indices = approx. 1 cm of distance
# 512 indices = 1 full rotation of the wheel or approx. 20.1 cm of distance
# 640 indices = 180 degree turn of the kart or approx. 25.1 cm of distance
# 320 indices = 90 degree turn of the kart or approx. 12.6 cm of distance
# ======================================================================
def drive(duration_indices, direction="forward"):
    for i in range(duration_indices):
        for step in range(8):
            for pin in range(4):
                if direction.lower() == "forward":
                    GPIO.output(gpio_pins_left[pin], active_coil_matrix[step][pin])         # Sequences like normal
                    GPIO.output(gpio_pins_right[pin], active_coil_matrix[4 - step][pin])    # Sequences in reverse, since right wheel is mirrored: net forward
                elif direction.lower() == "back":
                    GPIO.output(gpio_pins_left[pin], active_coil_matrix[4 - step][pin])     # Reverse of the
                    GPIO.output(gpio_pins_right[pin], active_coil_matrix[step][pin])        # forward movement
                elif direction.lower() == "left":
                    GPIO.output(gpio_pins_right[pin], active_coil_matrix[4 - step][pin])    # Left wheel does not spin so the kart arcs left
                elif direction.lower() == "right":
                    GPIO.output(gpio_pins_left[pin], active_coil_matrix[step][pin])         # Right wheel does not spin so the kart arcs right
                elif direction.lower() == "rotate":
                    GPIO.output(gpio_pins_left[pin], active_coil_matrix[4 - step][pin])     # Both wheels moving opposite direction,
                    GPIO.output(gpio_pins_right[pin], active_coil_matrix[4 - step][pin])    # Causing the kart to turn in its place
            time.sleep(0.001)

if __name__ == '__main__':
    main()
