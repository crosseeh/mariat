# ======================================================================
# This class is a template for a killer kart. For
# Now it can only move (forward, backward, left, right,
# rotate in place). It has a function test_drive that
# sets out a course and tests all these functions.
# Commented next to test_drive are the movement predictions.
#
# TODO:
# add button effects
# add behavior
# optional: add optical effects
# optional: add audio effects
# ...
#
# =============== For indices (parameter in spin_wheel() ===============
# 100 indices = approx. 1 second
# 24 indices = approx. 1 cm of distance
# 512 indices = 1 full rotation of the wheel or approx. 20.1 cm of distance
# 640 indices = 180 degree turn of the kart or approx. 25.1 cm of distance
# 320 indices = 90 degree turn of the kart or approx. 12.6 cm of distance
# 160 indices = 45 degree turn of the kart or approx. 6.3 cm of distance
# ======================================================================

import RPi.GPIO as GPIO
import time

class Kart:
    def __init__(self):
        # READ IN BCM: THESE ARE THE GPIO NUMBERS
        # NOT THE PIN NUMBERS ON THE RASPBERRY PI
        self.gpio_pins_left = [2, 3, 4, 17]
        self.gpio_pins_right = [27, 22, 10, 9]
        self.active_coil_matrix = [ [1, 0, 0, 0],   # Sequence in which coils in the electric
                                    [1, 1, 0, 0],   # motor are powered. The elements represent
                                    [0, 1, 0, 0],   # the pins in gpio_pins_(left/right).
                                    [0, 1, 1, 0],
                                    [0, 0, 1, 0],
                                    [0, 0, 1, 1],
                                    [0, 0, 0, 1],
                                    [1, 0, 0, 1] ]
        self.net_x = 0
        self.net_y = 0

    # Initializes the GPIO pins connected to the motor as an output
    def setup(self):
        GPIO.setmode(GPIO.BCM)
        gpio_pins = [2, 3, 4, 17, 27, 22, 10, 9]
        for pin in gpio_pins:
            GPIO.setup(pin, GPIO.OUT)

    def update_position(facing_to):
        if facing_to == "forward":
            self.net_y += 1
        elif facing_to == "forward_left":
            self.net_x -= 0.75
            self.net_y += 0.75
        elif facing_to == "left":
            self.net_x -= 1
        elif facing_to == "back_left":
            self.net_x -= 0.75
            self.net_y -= 0.75
        elif facing_to == "back":
            self.net_y -= 1
        elif facing_to == "back_right":
            self.net_x += 0.75
            self.net_y -= 0.75
        elif facing_to == "right":
            self.net_x += 1
        elif facing_to == "forward_right":
            self.net_x += 0.75
            self.net_y += 0.75
        else:
            self.net_x += 0
            self.net_y += 0

    # Makes a wheel spin in a certain direction (clockwise / counterclockwise)
    def drive(self, indices, direction="forward", facing=""):
        self.setup()
        for i in range(indices):
            for step in range(8):
                for pin in range(4):
                    if direction.lower() == "forward":
                        GPIO.output(self.gpio_pins_left[pin], self.active_coil_matrix[4 - step][pin])         # Sequences like normal
                        GPIO.output(self.gpio_pins_right[pin], self.active_coil_matrix[step][pin])            # Sequences in reverse, since right wheel is mirrored: net forward
                        update_position(facing)
                    elif direction.lower() == "back":                                                               
                        GPIO.output(self.gpio_pins_left[pin], self.active_coil_matrix[step][pin])             # Reverse of the
                        GPIO.output(self.gpio_pins_right[pin], self.active_coil_matrix[4 - step][pin])        # forward movement
                        update_position(facing)
                    elif direction.lower() == "rotate right":
                        GPIO.output(self.gpio_pins_left[pin], self.active_coil_matrix[4 - step][pin])         # Both wheels moving opposite direction,
                        GPIO.output(self.gpio_pins_right[pin], self.active_coil_matrix[4 - step][pin])        # causing the kart to turn in its place (right)
                        update_position(facing)
                    elif direction.lower() == "rotate left":
                        GPIO.output(self.gpio_pins_left[pin], self.active_coil_matrix[step][pin])             # Both wheels moving opposite direction,
                        GPIO.output(self.gpio_pins_right[pin], self.active_coil_matrix[step][pin])            # Causing the kart to turn in its place (left)
                        update_position(facing)
                time.sleep(0.00075)
        GPIO.cleanup()
