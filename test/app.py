# =====================================================
# Main control panel for the killerbot application. From
# here commands should be given to the kart. For now this
# is manual, but later a bluetooth remote will be implemented
# from which the kart will be controlled.
#
# TODO:
# add remote control
# ...
#
# =====================================================

from behavior import *
from KartMetController import *


def main():
    optie = input(" wil je bus 1 of 2")
    if(int(optie) == 1):
        battlebus = Behavior()
        battlebus.ai()
    elif(int(optie) == 2):
        battlebus = KartMetController()
        battlebus.move()

if __name__ == '__main__':
    main()
