
def cm_to_index(distance_in_cm):
    return int(distance_in_cm * 24)


def s_to_index(time_in_s):
    return int(time_in_s * 100)


def deg_to_index(angle_in_deg):
    return int(angle_in_deg * 3.5)


def index_to_cm(indices):
    return indices / 24


def index_to_s(indices):
    return indices / 100


def index_to_deg(indices):
    return indices / 3.5


def calc_velocity(indices):
    a = index_to_distance_cm(indices)
    b = index_to_time_s(indices)
    return a / b
