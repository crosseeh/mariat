import RPi.GPIO as GPIO
import time
import pygame
import os
import threading
import muziek

halfstep_seq = [
[1,0,0,0],
[1,1,0,0],
[0,1,0,0],
[0,1,1,0],
[0,0,1,0],
[0,0,1,1],
[0,0,0,1],
[1,0,0,1]
]
control_pinsL = [2,3,4,17]
control_pinsR = [9,10,22,27]

GPIO.setmode(GPIO.BCM)
GPIO.setup(16,GPIO.IN)
GPIO.setup(21,GPIO.IN)
GPIO.setup(12,GPIO.IN)
    
for pin in control_pinsL:
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, 0)


for pin in control_pinsR:
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, 0)


def sensoren():


    achter = 0
    links = 0
    rechts = 0
    prev_input = 0
    
    input = GPIO.input(12)
    if rechts == 0:
        if not ((not prev_input) and input):
            rechts = 1
            prev_input = input
            time.sleep(0.05)
            print(rechts,"R0")
            lampaan()
    input = GPIO.input(12)

    if rechts == 1:
        if not ((not prev_input) and input):
            rechts = 1
            prev_input = input
            time.sleep(0.05)
            print(rechts,"R1")
    input = GPIO.input(16)

    if links == 0:
        if not ((not prev_input) and input):
            links = 1
            prev_input = input
            time.sleep(0.05)
            print(links,"L0")
            lampaan()
    input = GPIO.input(16)

    if links == 1:
        if not ((not prev_input) and input):
            links = 1
            prev_input = input
            time.sleep(0.05)
            print(links,"L1")
    input =  GPIO.input(21)
    if achter == 0:
        if (not GPIO.input(21)):
            achter = 0
        else:
            achter = 1

    return rechts,links,achter

def achteruit():
    for i in range(300):
        for halfstep in range(8):
            for pin in range(4):
                GPIO.output(control_pinsL[pin], halfstep_seq[4- halfstep][pin])
                GPIO.output(control_pinsR[pin], halfstep_seq[halfstep][pin])
            time.sleep(0.001)


def vooruit():
    for i in range (700):
        rechts,links,achter = sensoren()
        while rechts == 0 and links == 0 and achter == 0:
            rechts,links,achter = sensoren()
            for halfstep in range(8):
                for pin in range(4):
                    GPIO.output(control_pinsL[pin], halfstep_seq[halfstep][pin])
                    GPIO.output(control_pinsR[pin], halfstep_seq[4 - halfstep][pin])
                time.sleep(0.001)
        else:
            if rechts == 1:
                lampaan()
                achteruit()
                bocht()
                bochtL()
                lampuit()

            if links == 1:
                lampaan()
                achteruit()
                bocht()
                bochtR()
                lampuit()


def bochtR():
    for i in range(512):
        for halfstep in range(8):
            for pin in range(4):
                GPIO.output(control_pinsL[pin], halfstep_seq[halfstep][pin])
            time.sleep(0.001)

def bochtL():
    for i in range(512):
        for halfstep in range(8):
            for pin in range(4):
                GPIO.output(control_pinsR[pin], halfstep_seq[4 - halfstep][pin])
            time.sleep(0.001)
#------------------------------------------------------------
def lampaan():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(20,GPIO.OUT)
    GPIO.output(20,1)
    PIN = 20
    

def lampuit():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(20,GPIO.OUT)
    GPIO.output(20,0)
    PIN = 20
 #-------------------------------------------------------      
        
#--------------------------------------------------------------------------
    # Define some colors.
BLACK = pygame.Color('black')
WHITE = pygame.Color('white')

# READ IN BCM: THESE ARE THE GPIO NUMBERS
# NOT THE PIN NUMBERS ON THE RASPBERRY PI
gpio_pins_left = [2, 3, 4, 17]
gpio_pins_right = [27, 22, 10, 9]
active_coil_matrix = [ [1, 0, 0, 0],   # Sequence in which coils in the electric
                            [1, 1, 0, 0],   # motor are powered. The elements represent
                            [0, 1, 0, 0],   # the pins in gpio_pins_(left/right).
                            [0, 1, 1, 0],
                            [0, 0, 1, 0],
                            [0, 0, 1, 1],
                            [0, 0, 0, 1],
                            [1, 0, 0, 1] ]

# This is a simple class that will help us print to the screen.
# It has nothing to do with the joysticks, just outputting the
# information.
class TextPrint(object):
    def __init__(self):
        self.reset()
        self.font = pygame.font.Font(None, 20)

    def tprint(self, screen, textString):
        textBitmap = self.font.render(textString, True, BLACK)
        screen.blit(textBitmap, (self.x, self.y))
        self.y += self.line_height

    def reset(self):
        self.x = 10
        self.y = 10
        self.line_height = 15

    def indent(self):
        self.x += 10

    def unindent(self):
        self.x -= 10


pygame.init()

# Set the width and height of the screen (width, height).
screen = pygame.display.set_mode((500, 700))

pygame.display.set_caption("My Game")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates.
clock = pygame.time.Clock()

# Initialize the joysticks.
pygame.joystick.init()

# Get ready to print.
textPrint = TextPrint()

def setup():
    GPIO.setmode(GPIO.BCM)
    gpio_pins = [2, 3, 4, 17, 27, 22, 10, 9]
    for pin in gpio_pins:
        GPIO.setup(pin, GPIO.OUT)

# Makes a wheel spin in a certain direction (clockwise / counterclockwise)
def drive(direction):
    setup()

    for i in range(64):
        for step in range(8):
            for pin in range(4):
                if direction.lower() == "rotate left":
                    print("links")
                    GPIO.output(gpio_pins_left[pin], active_coil_matrix[4 - step][pin])         # Sequences like normal
                    GPIO.output(gpio_pins_right[pin], active_coil_matrix[step][pin])      # Sequences in reverse, since right wheel is mirrored: net forward
                elif direction.lower() == "rotate right":
                    print("rechts")
                    GPIO.output(gpio_pins_left[pin], active_coil_matrix[step][pin])             # Reverse of the
                    GPIO.output(gpio_pins_right[pin], active_coil_matrix[4 - step][pin])        # forward movement
                elif direction.lower() == "back":
                    print("back")
                    GPIO.output(gpio_pins_left[pin], active_coil_matrix[4 - step][pin])         # Both wheels moving opposite direction,
                    GPIO.output(gpio_pins_right[pin], active_coil_matrix[4 - step][pin])        # causing the kart to turn in its place (right)
                elif direction.lower() == "forward":
                    print("voor")
                    GPIO.output(gpio_pins_left[pin], active_coil_matrix[step][pin])             # Both wheels moving opposite direction,
                    GPIO.output(gpio_pins_right[pin], active_coil_matrix[step][pin])            # Causing the kart to turn in its place (left)
                    main()
            time.sleep(0.00075)


def knop():
    klaar = False
    while klaar == False:
        setup()
        for event in pygame.event.get(): # User did something.
             if event.type == pygame.JOYBUTTONDOWN: # If user clicked close.
                klaar = True
        joystick_count = pygame.joystick.get_count()

        textPrint.tprint(screen, "Number of joysticks: {}".format(joystick_count))
        textPrint.indent()

        # For each joystick:
        for i in range(joystick_count):
            joystick = pygame.joystick.Joystick(i)
            joystick.init()

            

                

# -------- Main Program Loop -----------
def main2():
    knop()
    print("fakka")
    for i in range (700):
        
        for halfstep in range(8):
            for pin in range(4):
                GPIO.output(control_pinsL[pin], halfstep_seq[halfstep][pin])
                GPIO.output(control_pinsR[pin], halfstep_seq[4 - halfstep][pin])
            time.sleep(0.001)
    done = False
    
    while not done:
        setup()

        #
        # EVENT PROCESSING STEP
        #
        # Possible joystick actions: JOYAXISMOTION, JOYBALLMOTION, JOYBUTTONDOWN,
        # JOYBUTTONUP, JOYHATMOTION
        for event in pygame.event.get(): # User did something.
            if event.type == pygame.JOYBUTTONDOWN: # If user clicked close.
                print("waarom")
            elif event.type == pygame.QUIT:
                main()
            elif event.type == pygame.JOYBUTTONUP:
                print("Joystick button released.")
        

        #
        # DRAWING STEP
        #
        # First, clear the screen to white. Don't put other drawing commands
        # above this, or they will be erased with this command.
        screen.fill(WHITE)
        textPrint.reset()

        # Get count of joysticks.
        joystick_count = pygame.joystick.get_count()

        textPrint.tprint(screen, "Number of joysticks: {}".format(joystick_count))
        textPrint.indent()

        # For each joystick:
        for i in range(joystick_count):
            joystick = pygame.joystick.Joystick(i)
            joystick.init()

            textPrint.tprint(screen, "Joystick {}".format(i))
            textPrint.indent()

            # Get the name from the OS for the controller/joystick.
            name = joystick.get_name()
            textPrint.tprint(screen, "Joystick name: {}".format(name))

            # Usually axis run in pairs, up/down for one, and left/right for
            # the other.
            axes = joystick.get_numaxes()
            textPrint.tprint(screen, "Number of axes: {}".format(axes))
            textPrint.indent()

            for i in range(axes):
                axis = joystick.get_axis(i)
                textPrint.tprint(screen, "Axis {} value: {:>6.3f}".format(i, axis))
            textPrint.unindent()

            buttons = joystick.get_numbuttons()
            textPrint.tprint(screen, "Number of buttons: {}".format(buttons))
            textPrint.indent()

            for i in range(buttons):
                button = joystick.get_button(i)
                textPrint.tprint(screen,
                                 "Button {:>2} value: {}".format(i, button))
            textPrint.unindent()

            hats = joystick.get_numhats()
            textPrint.tprint(screen, "Number of hats: {}".format(hats))
            textPrint.indent()

            # Hat position. All or nothing for direction, not a float like
            # get_axis(). Position is a tuple of int values (x, y).
            for i in range(hats):
                hat = joystick.get_hat(i)
                # Rotate to the left
                if hat == (0, -1):
                    print(hat)
                    drive("back")
                # Rotate to the right
                elif hat == (1, 0):
                    print(hat)
                    drive("rotate right")
                # Move forward
                elif hat == (0, 1):
                    print(hat)
                    drive("forward")
                # Move backward
                elif hat == (-1, 0):
                    print(hat)
                    drive("rotate left")

            textPrint.unindent()

        #
        # ALL CODE TO DRAW SHOULD GO ABOVE THIS COMMENT
        #

        # Go ahead and update the screen with what we've drawn.
        pygame.display.flip()

        # Limit to 20 frames per second.
        clock.tick(20)

# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
#pygame.quit()
def main():
    lampuit()
    eind = 0
    rechts,links,achter = sensoren()
    while achter == 0:
    
        rechts,links,achter = sensoren()
        while rechts == 0 and links == 0 and achter == 0:
            rechts,links,achter = sensoren()
            vooruit()
            rechts,links,achter = sensoren()

            
        if rechts ==1:
            achteruit()
            bochtL()
        if links == 1:
            achteruit()
            bochtR()

    if achter == 1:
        ballon()
        while achter == 1:
            
            lampaan()
            rechts,links,achter = sensoren()
            for halfstep in range(8):
                for pin in range(4):
                    GPIO.output(control_pinsL[pin], halfstep_seq[halfstep][pin])
                    GPIO.output(control_pinsR[pin], halfstep_seq[4 - halfstep][pin])
                time.sleep(0.001)
            
            if  eind < 1:
                if rechts == 1 and links == 0:
                    lampaan()
                    time.sleep(1)
                    lampuit()
                    bochtL()
                    eind = eind + 1
                if links == 1 and rechts == 0:
                    lampaan()
                    time.sleep(1)
                    lampuit()
                    bochtR()
                    eind = eind + 1
            else:
                if rechts == 1 or links == 1:
                    lampuit()
                    GPIO.cleanup()
                    return




thread1 = threading.Thread(target=main2)
#thread2 = threading.Thread(target=muziek.play)
thread1.start()
#thread2.start()

