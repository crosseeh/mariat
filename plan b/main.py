import RPi.GPIO as GPIO
import unit_conversion
import effects
from time import sleep
from random import randint

# READ IN BCM: THESE ARE THE GPIO NUMBERS
# NOT THE PIN NUMBERS ON THE RASPBERRY PI
gpio_pins_left = [2, 3, 4, 17]
gpio_pins_right = [27, 22, 10, 9]
active_coil_matrix = [ [1, 0, 0, 0], # Sequence in which coils in the electric
                       [1, 1, 0, 0],   # motor are powered. The elements represent
                       [0, 1, 0, 0],   # the pins in gpio_pins_(left/right).
                       [0, 1, 1, 0],
                       [0, 0, 1, 0],
                       [0, 0, 1, 1],
                       [0, 0, 0, 1],
                       [1, 0, 0, 1] ]


def setup_pins():
    GPIO.setmode(GPIO.BCM)
    gpio_pins = [2, 3, 4, 17, 27, 22, 10, 9]
    for pin in gpio_pins:
        GPIO.setup(pin, GPIO.OUT)


def update_wheel(direction):
    for step in range(8):
        for pin in range(4):
            if direction.lower() == "forward":
                GPIO.output(gpio_pins_left[pin], active_coil_matrix[4 - step][pin])         # Sequences like normal
                GPIO.output(gpio_pins_right[pin], active_coil_matrix[step][pin])            # Sequences in reverse, since right wheel is mirrored: net forward
                interrupt = button_update()
                if interrupt:
                    return
                else:
                    continue
            elif direction.lower() == "back":
                GPIO.output(gpio_pins_left[pin], active_coil_matrix[step][pin])             # Reverse of the
                GPIO.output(gpio_pins_right[pin], active_coil_matrix[4 - step][pin])        # forward movement
                interrupt = button_update()
                if interrupt:
                    return
                else:
                    continue
        sleep(0.00075)
    return


def drive(direction, distance=0):
    setup_pins()
    if distance == 0:
        while True:
            update_wheel(direction)
    else:
        for i in range(distance):
            update_wheel(direction)
    GPIO.cleanup()


def rotate_left(angle):
    for i in range(angle):
        for step in range(8):
            for pin in range(4):
                GPIO.output(gpio_pins_left[pin], active_coil_matrix[step][pin])             # Both wheels moving opposite direction,
                GPIO.output(gpio_pins_right[pin], active_coil_matrix[step][pin])            # Causing the kart to turn in its place (left)
                # TODO luister naar button input

def rotate_right(angle):
    for i in range(angle):
        for step in range(8):
            for pin in range(4):
                GPIO.output(gpio_pins_left[pin], active_coil_matrix[4 - step][pin])         # Both wheels moving opposite direction,
                GPIO.output(gpio_pins_right[pin], active_coil_matrix[4 - step][pin])        # causing the kart to turn in its place (right)
                # TODO luister naar button input


def setup_button_pins():
    pins = [21, 12, 16]
    for pin in pins:
        GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)


def button_update():
    # TODO dit is de functie waar de button interrupt berekend wordt MAAR 1 KEER
    # initieer de button pins ook hier pas alsjeblieft
    setup_button_pins()
    backButtonPin = 21
    rightFrontButtonPin = 12
    leftFrontButtonPin = 16

    if GPIO.input(backButtonPin) == 1:
        eind()
        # TODO Max: Geluidje voor de eindshit intyfen:
        return True
    elif GPIO.input(leftFrontButtonPin == 1 or rightFrontButtonPin == 1):
        # TODO Gijs: delay fixen
        drive(unit_conversion.cm_to_index(10), "back")
        rotate_left(unit_conversion.deg_to_index(randint(30, 90)))
        return True
    elif GPIO.input(leftFrontButtonPin == 1 and rightFrontButtonPin == 1):
        drive(unit_conversion.cm_to_index(10), "back")
        rotate_left(unit_conversion.deg_to_index(randint(30, 90)))
        return True
    else:
        return False
    


def start():
    # user input
    drive("forward")
    while True:                                 # pseudo: blijf dit uitvoeren tot de button interrupt
        interrupt = button_update()
        interrupt = drive("forward")
        if interrupt == True:
            continue
        else:
            break
    eind()


def eind():
    print('joejoe ik ga naar de rand doei')
    drive("forward")


def main():
    start()


if __name__ == '__main__':
    main()
