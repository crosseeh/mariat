#!/bin/python
 
# importeer de GPIO bibliotheek.
import RPi.GPIO as GPIO
# Importeer de time biblotheek voor tijdfuncties.
from time import sleep
 
# Zet de pinmode op Broadcom SOC.
GPIO.setmode(GPIO.BCM)
# Zet waarschuwingen uit.
GPIO.setwarnings(False)
 
# Zet de GPIO pin als ingang.
GPIO.setup(17, GPIO.IN)
 
try:
  while True:
    # Wacht 1/4 van een seconde
    sleep(.250)
    # Lees de status van de GPIO pin uit.
    if GPIO.input(22):
      # Schakelaar is AAN.
      print "Schakelaar AAN, GPIO status:", GPIO.input(22)
    else:
      # Schakelaar is UIT.
      print "Schakelaar UIT, GPIO status:", GPIO.input(22)
 
except KeyboardInterrupt:  
  # GPIO netjes afsluiten
  GPIO.cleanup()