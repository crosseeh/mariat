# ======================================================================
# This class is a template for a killer kart. For
# Now it can only move (forward, backward, left, right,
# rotate in place). It has a function test_drive that
# sets out a course and tests all these functions.
# Commented next to test_drive are the movement predictions.
#
# TODO:
# add button effects
# add behavior
# optional: add optical effects
# optional: add audio effects
# ...
#
# =============== For indices (parameter in spin_wheel() ===============
# 100 indices = approx. 1 second
# 24 indices = approx. 1 cm of distance
# 512 indices = 1 full rotation of the wheel or approx. 20.1 cm of distance
# 640 indices = 180 degree turn of the kart or approx. 25.1 cm of distance
# 320 indices = 90 degree turn of the kart or approx. 12.6 cm of distance
# 160 indices = 45 degree turn of the kart or approx. 6.3 cm of distance
# ======================================================================

import RPi.GPIO as GPIO
import time
import pygame

class KartMetController:
    def __init__(self):
        # Initialize pygame
        pygame.init()
        # Initialize the joysticks.
        pygame.joystick.init()

        # READ IN BCM: THESE ARE THE GPIO NUMBERS
        # NOT THE PIN NUMBERS ON THE RASPBERRY PI
        self.gpio_pins_left = [2, 3, 4, 17]
        self.gpio_pins_right = [27, 22, 10, 9]
        self.active_coil_matrix = [ [1, 0, 0, 0],   # Sequence in which coils in the electric
                                    [1, 1, 0, 0],   # motor are powered. The elements represent
                                    [0, 1, 0, 0],   # the pins in gpio_pins_(left/right).
                                    [0, 1, 1, 0],
                                    [0, 0, 1, 0],
                                    [0, 0, 1, 1],
                                    [0, 0, 0, 1],
                                    [1, 0, 0, 1] ]

    # Initializes the GPIO pins connected to the motor as an output
    def setup(self):
        GPIO.setmode(GPIO.BCM)
        gpio_pins = [2, 3, 4, 17, 27, 22, 10, 9]
        for pin in gpio_pins:
            GPIO.setup(pin, GPIO.OUT)


    # Makes a wheel spin in a certain direction (clockwise / counterclockwise)
    def drive(self, direction):
        self.setup()
        for i in range(64):
            for step in range(8):
                for pin in range(4):
                    if direction.lower() == "forward":
                        GPIO.output(self.gpio_pins_left[pin], self.active_coil_matrix[4 - step][pin])         # Sequences like normal
                        GPIO.output(self.gpio_pins_right[pin], self.active_coil_matrix[step][pin])            # Sequences in reverse, since right wheel is mirrored: net forward
                    elif direction.lower() == "back":
                        GPIO.output(self.gpio_pins_left[pin], self.active_coil_matrix[step][pin])             # Reverse of the
                        GPIO.output(self.gpio_pins_right[pin], self.active_coil_matrix[4 - step][pin])        # forward movement
                    elif direction.lower() == "rotate right":
                        GPIO.output(self.gpio_pins_left[pin], self.active_coil_matrix[4 - step][pin])         # Both wheels moving opposite direction,
                        GPIO.output(self.gpio_pins_right[pin], self.active_coil_matrix[4 - step][pin])        # causing the kart to turn in its place (right)
                    elif direction.lower() == "rotate left":
                        GPIO.output(self.gpio_pins_left[pin], self.active_coil_matrix[step][pin])             # Both wheels moving opposite direction,
                        GPIO.output(self.gpio_pins_right[pin], self.active_coil_matrix[step][pin])            # Causing the kart to turn in its place (left)
                time.sleep(0.00075)


    def move(self):
        self.setup()
        # Loop until the user clicks the close button.
        done = False
        joystick_count = pygame.joystick.get_count()
        # -------- Main Program Loop -----------
        while not done:
            #
            # EVENT PROCESSING STEP
            #
            # Possible joystick actions: JOYAXISMOTION, JOYBALLMOTION, JOYBUTTONDOWN,
            # JOYBUTTONUP, JOYHATMOTION
            for event in pygame.event.get(): # User did something.
                if event.type == pygame.QUIT: # If user clicked close.
                    done = True # Flag that we are done so we exit this loop.
                elif event.type == pygame.JOYBUTTONDOWN:
                    print("Joystick button pressed.")
                elif event.type == pygame.JOYBUTTONUP:
                    print("Joystick button released.")


            # Get count of joysticks.
            joystick_count = pygame.joystick.get_count()

            # For each joystick:
            for i in range(joystick_count):
                joystick = pygame.joystick.Joystick(i)
                joystick.init()

                # Get the name from the OS for the controller/joystick.
                name = joystick.get_name()

                # Usually axis run in pairs, up/down for one, and left/right for
                # the other.
                axes = joystick.get_numaxes()

                for i in range(axes):
                    axis = joystick.get_axis(i)

                buttons = joystick.get_numbuttons()

                for i in range(buttons):
                    button = joystick.get_button(i)

                hats = joystick.get_numhats()

                # Hat position. All or nothing for direction, not a float like
                # get_axis(). Position is a tuple of int values (x, y).
                for i in range(hats):
                    hat = joystick.get_hat(i)
                    # Rotate to the left
                    if hat == (-1, 0):
                        print(hat)
                        self.drive("rotate left")
                    # Rotate to the right
                    elif hat == (1, 0):
                        print(hat)
                        self.drive("rotate right")
                    # Move forward
                    elif hat == (0, 1):
                        print(hat)
                        self.drive("forward")
                    # Move backward
                    elif hat == (0, -1):
                        print(hat)
                        self.drive("back")
