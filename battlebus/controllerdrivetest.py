import pygame
import RPi.GPIO as GPIO
import time
import os

# Define some colors.
BLACK = pygame.Color('black')
WHITE = pygame.Color('white')

# READ IN BCM: THESE ARE THE GPIO NUMBERS
# NOT THE PIN NUMBERS ON THE RASPBERRY PI
gpio_pins_left = [2, 3, 4, 17]
gpio_pins_right = [27, 22, 10, 9]
active_coil_matrix = [ [1, 0, 0, 0],   # Sequence in which coils in the electric
                            [1, 1, 0, 0],   # motor are powered. The elements represent
                            [0, 1, 0, 0],   # the pins in gpio_pins_(left/right).
                            [0, 1, 1, 0],
                            [0, 0, 1, 0],
                            [0, 0, 1, 1],
                            [0, 0, 0, 1],
                            [1, 0, 0, 1] ]

# This is a simple class that will help us print to the screen.
# It has nothing to do with the joysticks, just outputting the
# information.
class TextPrint(object):
    def _init_(self):
        self.reset()
        self.font = pygame.font.Font(None, 20)

    def tprint(self, screen, textString):
        textBitmap = self.font.render(textString, True, BLACK)
        screen.blit(textBitmap, (self.x, self.y))
        self.y += self.line_height

    def reset(self):
        self.x = 10
        self.y = 10
        self.line_height = 15

    def indent(self):
        self.x += 10

    def unindent(self):
        self.x -= 10


pygame.init()

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates.
clock = pygame.time.Clock()

# Initialize the joysticks.
pygame.joystick.init()

# Get ready to print.
textPrint = TextPrint()

def setup():
    GPIO.setmode(GPIO.BCM)
    gpio_pins = [2, 3, 4, 17, 27, 22, 10, 9]
    for pin in gpio_pins:
        GPIO.setup(pin, GPIO.OUT)

# Makes a wheel spin in a certain direction (clockwise / counterclockwise)
def drive(direction):
    setup()

    for i in range(64):
        for step in range(8):
            for pin in range(4):
                if direction.lower() == "forward":
                    GPIO.output(gpio_pins_left[pin], active_coil_matrix[4 - step][pin])         # Sequences like normal
                    GPIO.output(gpio_pins_right[pin], active_coil_matrix[step][pin])            # Sequences in reverse, since right wheel is mirrored: net forward
                elif direction.lower() == "back":
                    GPIO.output(gpio_pins_left[pin], active_coil_matrix[step][pin])             # Reverse of the
                    GPIO.output(gpio_pins_right[pin], active_coil_matrix[4 - step][pin])        # forward movement
                elif direction.lower() == "rotate right":
                    GPIO.output(gpio_pins_left[pin], active_coil_matrix[4 - step][pin])         # Both wheels moving opposite direction,
                    GPIO.output(gpio_pins_right[pin], active_coil_matrix[4 - step][pin])        # causing the kart to turn in its place (right)
                elif direction.lower() == "rotate left":
                    GPIO.output(gpio_pins_left[pin], active_coil_matrix[step][pin])             # Both wheels moving opposite direction,
                    GPIO.output(gpio_pins_right[pin], active_coil_matrix[step][pin])            # Causing the kart to turn in its place (left)
            time.sleep(0.00075)

# -------- Main Program Loop -----------
def main():
    done = False

    while not done:
        setup()
        # Get count of joysticks.
        joystick_count = pygame.joystick.get_count()

        # For each joystick:
        for i in range(joystick_count):
            joystick = pygame.joystick.Joystick(i)
            joystick.init()


            # Get the name from the OS for the controller/joystick.
            name = joystick.get_name()

            # Usually axis run in pairs, up/down for one, and left/right for
            # the other.
            axes = joystick.get_numaxes()

            for i in range(axes):
                axis = joystick.get_axis(i)

            buttons = joystick.get_numbuttons()

            for i in range(buttons):
                button = joystick.get_button(i)

            hats = joystick.get_numhats()

            # Hat position. All or nothing for direction, not a float like
            # get_axis(). Position is a tuple of int values (x, y).
            for i in range(hats):
                hat = joystick.get_hat(i)
                print(hat)
                # Rotate to the left
                if hat == (-1, 0):
                    print(hat)
                    drive("rotate left")
                # Rotate to the right
                elif hat == (1, 0):
                    print(hat)
                    drive("rotate right")
                # Move forward
                elif hat == (0, 1):
                    print(hat)
                    drive("forward")
                # Move backward
                elif hat == (0, -1):
                    print(hat)
                    drive("back")


        #
        # ALL CODE TO DRAW SHOULD GO ABOVE THIS COMMENT
        #

        # Limit to 20 frames per second.
        clock.tick(20)

# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
#pygame.quit()
if __name__ == "__main__":
    main()
