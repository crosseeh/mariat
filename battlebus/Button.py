from time import sleep
import RPi.GPIO as GPIO

class Button:
    def __init__(self, pin, button_id):
        self.pin = pin
        self.button_id = button_id
        
    def setup(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def listen_for_input(self):
        button = True
        while button:
            if GPIO.input(self) == 0:
                print("Button " + self.button_id + " pressed")
                self.func(*self.args)
                break;
