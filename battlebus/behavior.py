
import unit_conversion
import Kart
import Button
from time import sleep
from random import randint


class Behavior:
    def __init__(self):
        self.kart = Kart()
        self.directions = ["forward", "forward_left", "left", "back_left", "back", "back_right", "right", "forward_right"]
        self.recovery_dict = {"forward": 90, "forward_left": 135, "left": 180, "back_left": 225, "back": 270, "back_right": 315, "right": 0, "forward_right": 45}
        self.kart_facing = ""
        self.button_left = Button(16, 1)
        self.button_right = Button(12, 2)
        self.button_back = Button(21, 3, quit_program())

    def ai(self):
        button_input = start()
        if button_input == False:
            self.kart.drive(cm_to_index(10), "back", self.kart_facing)
            self.kart.drive(deg_to_index(90), "rotate left")
            self.kart_facing = "right"
            self.kart.drive(cm_to_index(50), "forward", self.kart_facing)
        behaviors = ["standard", "attack", "defend"]
        back_button = True
        while back_button:
            random_index = randint(0, len(behaviors) - 1)
            behavior = behaviors[random_index]
            if behavior == "standard":
                print("Standard behavior")
                back_button = self.standard()
            elif behavior == "attack":
                print("Attack behavior")
                back_button = self.attack()
            elif behavior == "defend":
                print("Defend behavior")
                back_button = self.defend()
            i += 1
        self.return_to_start()

<<<<<<< HEAD
    def start():
        print("user input")
        print("if input stops: ")
        self.kart.drive(cm_to_index(200), "forward", self.kart.facing)
=======

    def test():
        self.kart.drive(deg_to_index(90), "rotate right")
        self.kart_facing = "right"
        self.kart.drive(cm_to_index(50), "forward", self.kart_facing)
        back_button = True
        while back_button:
            back_button = self.standard()
        self.return_to_start()

>>>>>>> 458df6cb2cd953cda6a419f4642234fd238526ac

    def interrupt():
        interrupt = self.controlButtons.buttonClicks("back")
        return interrupt

    def return_to_start(self):
        x_index = int(self.kart.net_x / 32)
        y_index = int(self.kart.net_y / 32)
        # backtrack x en y index
        self.kart.drive(cm_to_index(5), "back", self.kart_facing)
        if x_index < 0:
            self.kart.drive(deg_to_index(self.recovery_dict[self.kart_facing]), "rotate right")
            self.kart_facing = "right"
            self.kart.drive(-x_index, "forward", self.kart_facing)
            if y_index < 0:
                self.kart.drive(deg_to_index(90), "rotate left")
                self.kart_facing = "forward"
                self.kart.drive(-y_index, "forward", self.kart_facing)
            elif y_index > 0:
                self.kart.drive(deg_to_index(90), "rotate right")
                self.kart_facing = "back"
                self.kart.drive(y_index, "forward", self.kart_facing)
        elif x_index > 0:
            self.kart.drive(deg_to_index(self.recovery_dict[self.kart_facing]), "rotate left")
            self.kart_facing = "left"
            self.kart.drive(x_index, "forward", self.kart_facing)
            if y_index < 0:
                self.kart.drive(deg_to_index(90), "rotate right")
                self.kart_facing = "forward"
                self.kart.drive(-y_index, "forward", self.kart_facing)
            elif y_index > 0:
                self.kart.drive(deg_to_index(90), "rotate left")
                self.kart_facing = "back"
                self.kart.drive(y_index, "forward", self.kart_facing)
        if self.kart_facing == "forward":
            self.kart.drive(deg_to_index(90), "rotate right")
        elif self.kart_facing == "back":
            self.kart.drive(deg_to_index(90), "rotate left")
        self.kart_facing = "right"
        self.kart.drive(cm_to_index(50), "forward")

    def standard(self):
        for i in range(8):
            index = self.directions.find(self.kart_facing)
            if index + 1 > 7:
                index = -1
            interrupt = self.kart.drive(deg_to_index(45), "rotate left")
            if interrupt = false:
                break
            self.kart_facing = self.directions[index + 1]
            interrupt = self.kart.drive(cm_to_index(10), "forward", self.kart_facing)   # verander afstand naar 100 ipv 10
        return True

    def attack(self):
        index = self.directions.index(self.kart_facing)
        for i in range(8):
            self.kart.drive(deg_to_index(45), "rotate left")
            if index + 1 > 7:
                index = -1
            self.kart_facing = self.directions[index + 1]
            index = self.directions.find(self.kart_facing)
            self.kart.drive(cm_to_index(50), "forward", self.kart_facing)
            self.kart.drive(deg_to_index(90), "rotate left")
            if index + 2 > 7 and index + 1 > 7:
                index = -1
            elif index + 2 > 7 and not index + 1 > 7:
                index = -2
            self.kart_facing = self.directions[index + 2]
            index = self.directions.find(self.kart_facing)
<<<<<<< HEAD
            self.kart.drive(cm_to_index(250), "forward", self.kart_facing)
=======

            self.kart.drive(cm_to_index(250), "forward", self.kart_facing)

>>>>>>> 458df6cb2cd953cda6a419f4642234fd238526ac
            self.kart.drive(deg_to_index(90), "rotate left")
            if index + 2 > 7 and index + 1 > 7:
                index = -1
            elif index + 2 > 7 and not index + 1 > 7:
                index = -2
            self.kart_facing = self.directions[index + 2]
            index = self.directions.find(self.kart_facing)
            self.kart.drive(cm_to_index(50), "forward", self.kart_facing)

    def defend(self):
        self.kart.drive(deg_to_index(90), "rotate left")
        self.kart_facing = "forward"
        time.sleep(5)
        self.kart.drive(deg_to_index(90), "rotate right")
        self.kart_facing = "right"
