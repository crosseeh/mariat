###########################################
##           DEZE GEBRUIKEN :)           ##  
###########################################                 

from gpiozero import LED
from time import sleep


############licht#################################
def ledgreen(): #DEZE RUNNEN ALS DE KART IN HET SPEL ZIT!!
    ledGroen = LED(24) #define green led
    ledstripG = LED(20) #define ledstrip green
    while True:
        ledGroen.on() #turn green led on while in game
        ledstripG.on()


def interactieWhenGameOver():
    ledRood  = LED(23) #define red led
    ledstripG = LED(20) #define red ledstrip
    ledRood.on()
    delay = 0.5
    for gameover in range(5):
        ledstripG.on() #red led on
        sleep(delay) #keep on for 0.5 second
        ledstripG.off() #red led on
        sleep(delay) #keep off for 0.5 second
        delay += 0.1
    ledstripG.off()


##############geluid######################
def GameOverSound():
    buzzer = LED(25) #define buzzer. Negeer dat er 'LED' staat, LED zorgt alleen dat er stroom op de GPIO pin komt.
    while True:
        for i in range(2):
            buzzer.on() #2 keer piepen
            sleep(0.3) #Wacht 3 millisecondes 
            buzzer.off()
        buzzer.on()#lange eindpiep
        sleep(2)
        buzzer.off() 

def reverse():
    buzzer = LED(25) #negeer dat er 'LED' staat, LED zorgt alleen dat er stroom op de GPIO pin komt.
    while True:
        buzzer.on()
        sleep(0.7)
        buzzer.off()
        sleep(1) 

interactieWhenGameOver()
reverse()


