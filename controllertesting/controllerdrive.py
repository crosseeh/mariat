import pygame
import RPi.GPIO as GPIO
import time

# GLOBAL VARS
# Define some colors.
BLACK = pygame.Color('black')
WHITE = pygame.Color('white')
pygame.init()
# Set the width and height of the screen (width, height).
screen = pygame.display.set_mode((500, 700))
pygame.display.set_caption("My Game")
# Used to manage how fast the screen updates.
clock = pygame.time.Clock()
# Initialize the joysticks.
pygame.joystick.init()
joystick_count = pygame.joystick.get_count()
gpio_pins_left = [2, 3, 4, 17]
gpio_pins_right = [27, 22, 10, 9]

active_coil_matrix = [[0, 1, 1, 1],  # Sequence in which coils in the electric
                      [0, 0, 1, 1],  # motor are powered. The elements represent
                      [1, 0, 1, 1],  # the pins in gpio_pins_(left/right).
                      [1, 0, 0, 1],
                      [1, 1, 0, 1],
                      [1, 1, 0, 0],
                      [1, 1, 1, 0],
                      [0, 1, 1, 0]]


# END OF GLOBAL VARS

def setup():
    GPIO.setmode(GPIO.BCM)
    gpio_pins = [2, 3, 4, 17, 27, 22, 10, 9]

    for pin in gpio_pins:
        GPIO.setup(pin, GPIO.OUT)


class TextPrint(object):
    def __init__(self):
        self.reset()
        self.font = pygame.font.Font(None, 20)

    def tprint(self, screen, textString):
        textBitmap = self.font.render(textString, True, BLACK)
        screen.blit(textBitmap, (self.x, self.y))
        self.y += self.line_height

    def reset(self):
        self.x = 10
        self.y = 10
        self.line_height = 15

    def indent(self):
        self.x += 10

    def unindent(self):
        self.x -= 10


def move():
    # Loop until the user clicks the close button.
    done = False
    # -------- Main Program Loop -----------
    while not done:
        #
        # EVENT PROCESSING STEP
        #
        # Possible joystick actions: JOYAXISMOTION, JOYBALLMOTION, JOYBUTTONDOWN,
        # JOYBUTTONUP, JOYHATMOTION
        for event in pygame.event.get():  # User did something.
            if event.type == pygame.QUIT:  # If user clicked close.
                done = True  # Flag that we are done so we exit this loop.
            elif event.type == pygame.JOYBUTTONDOWN:
                print("Joystick button pressed.")
            elif event.type == pygame.JOYBUTTONUP:
                print("Joystick button released.")

        for i in range(joystick_count):
            joystick = pygame.joystick.Joystick(i)
            joystick.init()
            hats = joystick.get_numhats()
            textPrint = TextPrint()

            # Hat position. All or nothing for direction, not a float like
            # get_axis(). Position is a tuple of int values (x, y).
            # (-1, 0 ) = links
            # (1, 0) = rechts
            # (0, 1) = omhoog
            # (0, -1) = omlaag
        for x in range(hats):
            hat = joystick.get_hat(x)
            textPrint.tprint(screen, "Hat {} value: {}".format(i, str(hat)))
            if hat == (-1, 0):
                print(hat)
                drive("rotate left")
            elif hat == (1, 0):
                print(hat)
                drive("rotate right")
            elif hat == (0, 1):
                print(hat)
                drive("forward")
            elif hat == (0, -1):
                print(hat)
                drive("back")

        buttons = joystick.get_numbuttons()
        textPrint.tprint(screen, "Number of buttons: {}".format(buttons))
        textPrint.indent()

        for i in range(buttons):
            button = joystick.get_button(i)
            textPrint.tprint(screen,
                             "Button {:>2} value: {}".format(i, button))
            if (i == 2 and button == 1):
                pygame.quit()

        textPrint.unindent()

        # Go ahead and update the screen with what we've drawn.
        pygame.display.flip()

        # Limit to 20 frames per second.
        clock.tick(20)

# Makes a wheel spin in a certain direction (clockwise / counterclockwise)
def drive(direction):
    print(direction)
    for step in range(8):
        for pin in range(4):
            if direction.lower() == "forward":
                print(direction)
                GPIO.output(gpio_pins_left[pin], active_coil_matrix[step][pin])         # Sequences like normal
                GPIO.output(gpio_pins_right[pin], active_coil_matrix[4 - step][pin])    # Sequences in reverse, since right wheel is mirrored: net forward
            elif direction.lower() == "back":
                print(direction)
                GPIO.output(gpio_pins_left[pin], active_coil_matrix[4 - step][pin])     # Reverse of the
                GPIO.output(gpio_pins_right[pin], active_coil_matrix[step][pin])        # forward movement
            elif direction.lower() == "rotate right":
                print(direction)
                GPIO.output(gpio_pins_left[pin], active_coil_matrix[step][pin])         # Both wheels moving opposite direction,
                GPIO.output(gpio_pins_right[pin], active_coil_matrix[step][pin])        # causing the kart to turn in its place (right)
            elif direction.lower() == "rotate left":
                print(direction)
                GPIO.output(gpio_pins_left[pin], active_coil_matrix[4 - step][pin])     # Both wheels moving opposite direction,
                GPIO.output(gpio_pins_right[pin], active_coil_matrix[4 - step][pin])    # Causing the kart to turn in its place (left)
        time.sleep(0.00075)



if __name__ == "__main__":
    setup()
    find_input()

    pygame.quit()
