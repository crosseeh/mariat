import pygame
import RPi.GPIO as GPIO
import time
import os

# Define some colors.
BLACK = pygame.Color('black')
WHITE = pygame.Color('white')

# READ IN BCM: THESE ARE THE GPIO NUMBERS
# NOT THE PIN NUMBERS ON THE RASPBERRY PI
gpio_pins_left = [2, 3, 4, 17]
gpio_pins_right = [27, 22, 10, 9]
active_coil_matrix = [ [1, 0, 0, 0],   # Sequence in which coils in the electric
                            [1, 1, 0, 0],   # motor are powered. The elements represent
                            [0, 1, 0, 0],   # the pins in gpio_pins_(left/right).
                            [0, 1, 1, 0],
                            [0, 0, 1, 0],
                            [0, 0, 1, 1],
                            [0, 0, 0, 1],
                            [1, 0, 0, 1] ]

def setup():
    GPIO.setmode(GPIO.BCM)
    gpio_pins = [2, 3, 4, 17, 27, 22, 10, 9]
    for pin in gpio_pins:
        GPIO.setup(pin, GPIO.OUT)

def drive(direction):
    setup()
    for i in range(300):
        for step in range(8):
            for pin in range(4):
                if direction.lower() == "forward":
                    GPIO.output(gpio_pins_left[pin], active_coil_matrix[4 - step][pin])         # Sequences like normal
                    GPIO.output(gpio_pins_right[pin], active_coil_matrix[step][pin])            # Sequences in reverse, since right wheel is mirrored: net forward
                elif direction.lower() == "back":
                    GPIO.output(gpio_pins_left[pin], active_coil_matrix[step][pin])             # Reverse of the
                    GPIO.output(gpio_pins_right[pin], active_coil_matrix[4 - step][pin])        # forward movement
                elif direction.lower() == "rotate right":
                    GPIO.output(gpio_pins_left[pin], active_coil_matrix[4 - step][pin])         # Both wheels moving opposite direction,
                    GPIO.output(gpio_pins_right[pin], active_coil_matrix[4 - step][pin])        # causing the kart to turn in its place (right)
                elif direction.lower() == "rotate left":
                    GPIO.output(gpio_pins_left[pin], active_coil_matrix[step][pin])             # Both wheels moving opposite direction,
                    GPIO.output(gpio_pins_right[pin], active_coil_matrix[step][pin])            # Causing the kart to turn in its place (left)
            time.sleep(0.00075)

# This is a simple class that will help us print to the screen.
# It has nothing to do with the joysticks, just outputting the
# information.
class TextPrint(object):
    def __init__(self):
        self.reset()
        self.font = pygame.font.Font(None, 20)

    def tprint(self, screen, textString):
        textBitmap = self.font.render(textString, True, BLACK)
        screen.blit(textBitmap, (self.x, self.y))
        self.y += self.line_height

    def reset(self):
        self.x = 10
        self.y = 10
        self.line_height = 15

    def indent(self):
        self.x += 10

    def unindent(self):
        self.x -= 10


pygame.init()

# Set the width and height of the screen (width, height).
screen = pygame.display.set_mode((500, 700))

pygame.display.set_caption("My Game")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates.
clock = pygame.time.Clock()

# Initialize the joysticks.
pygame.joystick.init()

# Get ready to print.
textPrint = TextPrint()
def check():
    # -------- Main Program Loop -----------
    while not done:
        #
        # EVENT PROCESSING STEP
        #
        # Possible joystick actions: JOYAXISMOTION, JOYBALLMOTION, JOYBUTTONDOWN,
        # JOYBUTTONUP, JOYHATMOTION
        for event in pygame.event.get(): # User did something.
            if event.type == pygame.QUIT: # If user clicked close.
                done = True # Flag that we are done so we exit this loop.
            elif event.type == pygame.JOYBUTTONDOWN:
                print("Joystick button pressed.")
            elif event.type == pygame.JOYBUTTONUP:
                print("Joystick button released.")

        #
        # DRAWING STEP
        #
        # First, clear the screen to white. Don't put other drawing commands
        # above this, or they will be erased with this command.
        screen.fill(WHITE)
        textPrint.reset()

        # Get count of joysticks.
        joystick_count = pygame.joystick.get_count()

        textPrint.tprint(screen, "Number of joysticks: {}".format(joystick_count))
        textPrint.indent()

        # For each joystick:
        for i in range(joystick_count):
            joystick = pygame.joystick.Joystick(i)
            joystick.init()

            textPrint.tprint(screen, "Joystick {}".format(i))
            textPrint.indent()

            # Get the name from the OS for the controller/joystick.
            name = joystick.get_name()
            textPrint.tprint(screen, "Joystick name: {}".format(name))

            # Usually axis run in pairs, up/down for one, and left/right for
            # the other.
            axes = joystick.get_numaxes()
            textPrint.tprint(screen, "Number of axes: {}".format(axes))
            textPrint.indent()

            for i in range(axes):
                axis = joystick.get_axis(i)
                textPrint.tprint(screen, "Axis {} value: {:>6.3f}".format(i, axis))
            textPrint.unindent()

            buttons = joystick.get_numbuttons()
            textPrint.tprint(screen, "Number of buttons: {}".format(buttons))
            textPrint.indent()

            for i in range(buttons):
                button = joystick.get_button(i)
                return True
                textPrint.tprint(screen,
                                "Button {:>2} value: {}".format(i, button))
            textPrint.unindent()

            hats = joystick.get_numhats()



            # Hat position. All or nothing for direction, not a float like
            # get_axis(). Position is a tuple of int values (x, y).
            # for i in range(hats):
            #     hat = joystick.get_hat(i)
            #     # Rotate to the left
            #     if hat == (-1, 0):
            #         drive("rotate left")
            #     # Rotate to the right
            #     elif hat == (1, 0):
            #         drive("rotate right")
            #     # Move forward
            #     elif hat == (0, 1):
            #         drive("forward")
            #     # Move backward
            #     elif hat == (0, -1):
            #         drive("back")

            textPrint.unindent()

        #
        # ALL CODE TO DRAW SHOULD GO ABOVE THIS COMMENT
        #
        # Go ahead and update the screen with what we've drawn.
        pygame.display.flip()
        return False

# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
pygame.quit()
